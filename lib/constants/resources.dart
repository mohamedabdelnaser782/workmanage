import 'package:flutter/material.dart';

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.black87,
  primary: Colors.grey[300],
  minimumSize: Size(88, 36),
  shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(80.0)),
  //textColor: Colors.white,
  textStyle: TextStyle(color: Colors.white),
  padding:  EdgeInsets.all(0),
);