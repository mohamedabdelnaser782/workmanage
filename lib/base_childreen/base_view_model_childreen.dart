import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rxdart/rxdart.dart';
import 'package:workmanager/util/providers_childreen/auth_provider.dart';
import 'package:workmanager/util/providers_childreen/fire_storage_provider.dart';
import 'package:workmanager/util/providers_childreen/fire_store_provider.dart';
import 'package:workmanager/util/services_childreen/auth_services.dart';
import 'package:workmanager/util/services_childreen/fire_storage_services.dart';
import 'package:workmanager/util/services_childreen/fire_store_services.dart';

import '../util/providers/fire_messaging_provider.dart';
import '../util/providers/fire_storage_provider.dart';
import '../util/providers/fire_store_provider.dart';
import '../util/services/auth_services.dart';
import '../util/services/fire_storage_services.dart';
import '../util/services/fire_store_services.dart';
import '../util/services/firestore_messing_service.dart';
export 'package:flutter_riverpod/flutter_riverpod.dart';
export 'package:rxdart/rxdart.dart';

abstract class BaseViewModelChildreen {
  BehaviorSubject<bool> bsLoading = BehaviorSubject.seeded(false);
  BehaviorSubject<bool> bsRunning = BehaviorSubject.seeded(false);

  final AutoDisposeProviderReference ref;
  late final FirestoreChildreenService firestoreService;
  late final AuthenticationServiceChildreen authChildreen;
  late final FireStorageChildreenService fireStorageService;
  late final FirestoreMessagingService firestoreMessagingService;
  User? user;

  BaseViewModelChildreen(this.ref) {
    authChildreen = ref.watch(authServicesChildreenProvider);
    user = authChildreen.currentUser();
    firestoreService = ref.watch(firestoreServicesChildreenProvider);
    fireStorageService = ref.watch(fireStorageChildreenServicesProvider);
    firestoreMessagingService = ref.watch(firestoreMessagingServiceProvider);
  }

  @mustCallSuper
  void dispose() {
    bsRunning.close();
    bsLoading.close();
  }

  showLoading() => bsLoading.add(true);

  closeLoading() => bsLoading.add(false);

  startRunning() => bsRunning.add(true);

  endRunning() => bsRunning.add(false);
}
