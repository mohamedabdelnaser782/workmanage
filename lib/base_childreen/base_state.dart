import 'package:flutter/material.dart';
import 'package:workmanager/base_childreen/base_view_model_childreen.dart';



export 'package:easy_localization/easy_localization.dart';
export 'package:flutter_riverpod/flutter_riverpod.dart';
export 'package:get/get.dart';

abstract class BaseStateChildreen<T extends StatefulWidget, V extends BaseViewModelChildreen>
    extends State<T> {
  bool onRunning = false;

  @override
  void initState() {
    super.initState();
    getVm().bsLoading.distinct((a, b) => a == b).listen((show) {
      if (show) {
        showLoading();
      } else {
        closeLoading();
      }
    });

    getVm().bsRunning.listen((run) {
      setState(() {
        onRunning = run;
      });
    });
  }

  void showLoading() {}

  void closeLoading() {}

  V getVm();
}
