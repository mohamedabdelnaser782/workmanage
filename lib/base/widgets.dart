import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:workmanager/models/meta_user_model.dart';
import 'package:workmanager/util/services/fire_store_services.dart';

class UserWidget extends StatelessWidget {
  final Widget Function(BuildContext context , MetaUserModel user) builder;

  const UserWidget({super.key, required this.builder});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
          stream:FirestoreService(FirebaseFirestore.instance).currentUserStream(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }else {
              MetaUserModel user =
              MetaUserModel.fromFirestore(snapshot.data!);
              return builder(context , user);
            }
          }
      ),
    );
  }

}