import 'package:cloud_firestore/cloud_firestore.dart';

import '../base/base_state.dart';

class NotificationModel {
  String ? title;
  String? body;
  String? uid;
  String?  createdAt ;
  NotificationModel(
      {required this.body,
        required this.title,
        required this.createdAt,
        required this.uid,
      });

  NotificationModel.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    title = json['title'];
    uid = json['uid'];
    createdAt = json['createdAt'];
  }

  String time(){
  return  DateTime.parse(createdAt!).day ==
        Timestamp.now().toDate().day
        ? DateFormat('hh:mm a ')
        .format(DateTime.parse(createdAt!))
        : DateFormat('dd MMM, hh:mm a  ')
        .format(DateTime.parse(createdAt!));
  }
  Map<String, dynamic> toMap() {
    return {
      'parent': body,
      'child': title,
      'uid': uid,
      'createdAt': createdAt,
    };
  }
}
