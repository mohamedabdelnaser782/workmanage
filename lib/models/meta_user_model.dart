import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class MetaUserModel extends Equatable {
  final String uid;
  final String email;
  final String displayName;
  final String? url;
  final String? token;
  final String? parent;

  const MetaUserModel({
    required this.email,
    required this.displayName,
    this.url,
    this.parent,
    this.uid = '',
    this.token = '',
  });

  factory MetaUserModel.fromFirestore(DocumentSnapshot doc) {
    print('Widget messageItem ${doc.id} => ${doc.data() as Map<String, dynamic>}');

    Map<String, dynamic> json = doc.data() as Map<String, dynamic>;

    return MetaUserModel(
      uid: doc.id,
      email: json['email'],
      parent: json['parent'],
      displayName: json['display_name'],
      url: json.containsKey('url') ? doc['url'] : null,
      token:json.containsKey('messingToken')
          ? doc['messingToken']
          : null,
    );
  }

  bool isChild()=>parent !=null && parent!.isNotEmpty ;

  @override
  List<Object?> get props => [uid];
}
