import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class MetaChildreenModel extends Equatable {
  final String uid;
  final String email;
  final String displayName;
  final String? url;
  final String? token;

  const MetaChildreenModel({
    required this.email,
    required this.displayName,
    this.url,
    this.uid = '',
    this.token = '',
  });

  factory MetaChildreenModel.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic> json = doc.data() as Map<String, dynamic>;

    return MetaChildreenModel(
      uid: doc.id,
      email: json['email'],
      displayName: json['display_name'],
      url: json.containsKey('url') ? doc['url'] : null,
      token:json.containsKey('messingToken')
          ? doc['messingToken']
          : null,
    );
  }

  @override
  List<Object?> get props => [uid];
}
