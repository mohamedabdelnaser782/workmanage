class AwardModel {
   String ?id  ;
   String ?  name ;
   bool ? status ;

  AwardModel(this.id, this.name, this.status);

   factory AwardModel.fromJson(Map<String, dynamic> json) {
     print('AwardModelfromJson $json');
    return AwardModel(json['id'], json['name'], json['status']);
  }

  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'id': id,
      'name': name,
    };
  }
}