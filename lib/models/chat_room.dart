class ChatRoom {
  String ? roomId;
  String? parent;
  String? child;
  String?  participant ;
  ChatRoom(
      {required this.roomId,
        required this.parent,


        required this.child,
       });

  ChatRoom.fromJson(Map<String, dynamic> json) {
    parent = json['parent'];
    child = json['child'];
  }

  Map<String, dynamic> toMap() {
    return {
      'parent': parent,
      'child': child,
    };
  }
  Map<String, dynamic> newRoomMap() {
    return {
      'parent': parent,
      'child': child,
      'users':[parent,child]
    };
  }
}
