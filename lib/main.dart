import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:workmanager/base/restart_app_widget.dart';
import 'app.dart';
import 'base/base_state.dart';
String kCurrentLang = 'EN';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ProviderScope(
      child: Consumer(
        builder: (context, watch, _) {
          return EasyLocalization(
            supportedLocales: const [Locale('en', 'US'), Locale('ar', 'JO')],
            path: 'assets/translations',
            startLocale: kCurrentLang =='EN' ? const Locale('en', 'US'): Locale('ar', 'JO'),
            fallbackLocale: kCurrentLang =='EN' ? const Locale('en', 'US'): Locale('ar', 'JO'),
            child: const App(),
          );
        },
      ),
    );
  }
}
