import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:workmanager/chat/chatting_children_screen.dart';
import 'package:workmanager/chat/message.dart';
import 'package:workmanager/models/chat_room.dart';
import 'package:workmanager/models/meta_childreen_model.dart';
import 'package:workmanager/models/meta_user_model.dart';

class ChatChildren extends StatelessWidget {
  //final CurrentAppUser currentuser;

  const ChatChildren({
    Key? key,
    //@required this.currentuser
  }) : super(key: key);
  Future<MetaUserModel> getCurrentUser() async {
    DocumentSnapshot<Map<String, dynamic>> doc = await FirebaseFirestore
        .instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .get();
    MetaUserModel currentUser = MetaUserModel.fromFirestore(doc);
    return currentUser;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Chats',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          elevation: 0,
          backgroundColor: Color(0xFF527DAA),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: SizedBox(
          child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance
                  .collection('chats')
                  .where('child',isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                  .snapshots(),
              builder: (context,  snap) {
                if (snap.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                List<String> ids = [];
              /*  snap.data!.doc.forEach((element) {
                  ids.add(element.data()['uid']);
                });*/
                List<ChatRoom> chatsList = [];
                snap.data!.docs.forEach((element) {
                  ChatRoom c = ChatRoom.fromJson(element.data() as Map<String,dynamic>);
                  c.roomId = element.id;
                  chatsList.add(c);
                });
                return chatsList.isEmpty
                    ? Center(
                        child: Text('No Chat available'),
                      )
                    : Column(
                        children: chatsList
                            .map((e) => messageItem(context, e))
                            .toList());
              }),
        ));
  }

  /*
  Widget messageItem(
    BuildContext context,
    ChatModel e,
    List<ChatModel> chatsList,
  ) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('doctors')
              .doc(e.docId)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            MetaChildreenModel user =
            MetaChildreenModel.fromFirestore(snapshot.data!.data());

            return ListTile(
              onTap: () {
                onMsgItemtab(user, context);
              },
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        user.displayName,
                      ),
                      Text(
                        timeago.format(DateTime.parse(e.dateTime!),
                            locale: 'en_long'),
                        style: const TextStyle(
                            fontSize: 9, fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width - 120,
                        child: Text(
                          e.lastMessage!,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w300),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              leading: CircleAvatar(
                radius: 20,
                backgroundColor: Color(0xFF527DAA),
                child: Icon(
                  Icons.account_circle,
                  size: 40,
                  color: Colors.white,
                ),
              ),
            );
          }),
    );
  }
*/
  Widget messageItem(
      BuildContext context,
      ChatRoom e) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('user')
              .doc(e.child)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            MetaChildreenModel user =
            MetaChildreenModel.fromFirestore(snapshot.data!);

            return ListTile(
              onTap: () {
                FirebaseFirestore.instance
                    .collection('chats')
                    .doc(e.roomId)
                    .update({'dr_seen': true});
                onMsgItemtab(user , e, context);
              },
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.displayName,
                  ),
                  StreamBuilder(
                      stream:
                      FirebaseFirestore.instance
                          .collection('chats')
                          .doc(e.roomId)
                          .collection('messages')
                          .orderBy('createdAt', descending: true)
                          .snapshots(),
                      builder: (context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        final message = snapshot.data?.docs.first;

                        print('chat_messages ${message?.data()}');
                        return message!=null ? messageTextItem(message ,context):Container();
                      }
                  ),

                ],
              ),
              leading: CircleAvatar(
                radius: 20,
                backgroundColor: Color(0xFF527DAA),
                child: Icon(
                  Icons.account_circle,
                  size: 40,
                  color: Colors.white,
                ),
              ),
            );
          }),
    );
  }


  Widget messageTextItem(DocumentSnapshot snapshot  , BuildContext context ) {
    final messageModel = Message.fromMap(snapshot.data() as Map<String , dynamic>);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            //width: MediaQuery.of(context).size.width - 120,
            child: Text(
              messageModel.message??'',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                  fontSize: 13, fontWeight: FontWeight.w300),
            ),

          ),
        ),
        messageModel.createdAt !=null ? Text(
          timeago.format(DateTime.parse(messageModel.createdAt!),
              locale: 'en_long'),
          style: const TextStyle(
              fontSize: 9, fontWeight: FontWeight.w300),
        ):Container()
      ],
    );
  }

  Future<void> onMsgItemtab(MetaChildreenModel e, ChatRoom room,BuildContext context) async {
    MetaUserModel u = await getCurrentUser();
    Navigator.push(context, MaterialPageRoute
      (builder: (context) => ChattingChildrenScreen(
        childreenModel: e,
        room: room,
        currentUser: u)));

  }
}
