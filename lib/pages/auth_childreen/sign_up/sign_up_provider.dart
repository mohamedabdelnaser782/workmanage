// ignore: depend_on_referenced_packages
import 'package:riverpod/riverpod.dart';

import 'sign_up_vm.dart';

final viewModelProvider = StateProvider.autoDispose<SignUpChildreenViewModel>(
  (ref) {
    var vm = SignUpChildreenViewModel(ref);
    ref.onDispose(() {
      vm.dispose();
    });
    return vm;
  },
);
