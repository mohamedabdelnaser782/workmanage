import 'package:flutter/material.dart';
import 'package:workmanager/base_childreen/base_state.dart';
import 'package:workmanager/constants/constants.dart';
import 'package:workmanager/routing/app_routes.dart';
import 'package:workmanager/util/extension/dimens.dart';
import 'package:workmanager/util/ui/common_widget/auth_switch.dart';
import 'package:workmanager/util/ui/common_widget/auth_switch_childreen.dart';
import 'package:workmanager/util/ui/common_widget/primary_button.dart';
import 'package:workmanager/util/ui/common_widget/sign_in_content.dart';

import '/base/base_state.dart';
import '/util/ui/common_widget/auth_text_field.dart';
import 'sign_up_provider.dart';
import 'sign_up_vm.dart';

class SignUpChildreenPage extends StatefulWidget {
  final ScopedReader watch;

  static Widget instance() {
    return Consumer(builder: (context, watch, _) {
      return SignUpChildreenPage._(watch);
    });
  }

  const SignUpChildreenPage._(this.watch);

  @override
  State<StatefulWidget> createState() {
    return SignUpChildreenState();
  }
}

class SignUpChildreenState extends BaseStateChildreen<SignUpChildreenPage, SignUpChildreenViewModel> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _fullNameController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String signUpChildreenStatusString = '';

  Future<void> _checkSignUpChildreen() async {
    if (_formKey.currentState!.validate()) {
      getVm().signUpChildreen(_emailController.text, _passwordController.text, _fullNameController.text);
    }
  }

  @override
  void initState() {
    super.initState();
    getVm().bsSignUpStatus.listen((value) {
      switch (value) {
        case SignUpChildreenStatus.successfulEmail:
          //().createData(_emailController.text, _fullNameController.text);
          break;
        case SignUpChildreenStatus.successfulData:
          Get.offAndToNamed(AppRoutes.HOME);
          break;
        case SignUpChildreenStatus.emailAlreadyInUse:
          setState(() {
            signUpChildreenStatusString = '';
          });
          break;
        case SignUpChildreenStatus.invalidEmail:
          setState(() {
            // signUpStatusString = AppStrings.i
          });
          break;
        default:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: SingleChildScrollView(
          child: buildForm(),
        ),
      ),
    );
  }

  Form buildForm() {
    return Form(
      key: _formKey,
      child: StreamBuilder<SignUpChildreenStatus>(
        stream: getVm().bsSignUpStatus,
        builder: (context, snapshot) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 86.w),
              SignInContent(
                title: StringTranslateExtension(AppStrings.createAccount).tr(),
                content: StringTranslateExtension(AppStrings.signUpDes).tr(),
              ),
              AuthTextField(
                controller: _fullNameController,
                label: StringTranslateExtension(AppStrings.fullName).tr(),
                hint: StringTranslateExtension(AppStrings.fullNameHint).tr(),
                validator: (val) => val!.length < 6
                    ? StringTranslateExtension(AppStrings.fullNameValid).tr()
                    : null,
                enabled: !onRunning,
              ),
              SizedBox(height: 32.w),
              AuthTextField(
                controller: _emailController,
                label: StringTranslateExtension(AppStrings.username).tr(),
                hint: StringTranslateExtension(AppStrings.usernameHint).tr(),
                validator: (val) => val!.isNotEmpty
                    ? null
                    : StringTranslateExtension(AppStrings.usernameValid).tr(),
                enabled: !onRunning,
              ),
              SizedBox(height: 32.w),
              AuthTextField(
                controller: _passwordController,
                label: StringTranslateExtension(AppStrings.password).tr(),
                hint: StringTranslateExtension(AppStrings.passwordHint).tr(),
                validator: (val) => val!.length < 6
                    ? StringTranslateExtension(AppStrings.passwordValid).tr()
                    : null,
                isPassword: true,
                enabled: !onRunning,
              ),
              const SizedBox(height: 60),
              Text(
                signUpChildreenStatusString,
                style: const TextStyle(
                  color: Colors.red,
                ),
              ).tr(),
              const SizedBox(height: 20),
              PrimaryButton(
                text: StringTranslateExtension(AppStrings.signUp).tr(),
                press: _checkSignUpChildreen,
                disable: !onRunning,
              ),
              const SizedBox(height: 20),
              const AuthSwitchChildreen(
                auth: authCaseChildreen.toSignInChildreen,
              ),
              const SizedBox(height: 20),
            ],
          );
        },
      ),
    );
  }

  @override
  SignUpChildreenViewModel getVm() => widget.watch(viewModelProvider).state;
}
