
import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:workmanager/base_childreen/base_view_model_childreen.dart';
import 'package:workmanager/models/chat_room.dart';

import '/base/base_view_model.dart';

class SignUpChildreenViewModel extends BaseViewModelChildreen {
  SignUpChildreenViewModel(ref) : super(ref);

  BehaviorSubject<SignUpChildreenStatus> bsSignUpStatus =
      BehaviorSubject.seeded(SignUpChildreenStatus.pause);

  String ? uID ;
  void signUpChildreen(String email, String password,String name) async {
    startRunning();
    bsSignUpStatus.add(SignUpChildreenStatus.runEmail);
    var status = await authChildreen.signUpChildreen(email, password, name, onSuccess: (String uid) {
      createData(email, name, uid);
    });
    bsSignUpStatus.add(status);
    endRunning();
  }

  void createData(String email, String name ,String uid) async {
    startRunning();
    user = authChildreen.currentUser();
    await firestoreService.createChildreenData(uid!, name, email,user!.email.toString());
    bsSignUpStatus.add(SignUpChildreenStatus.successfulData);
    createChatRoom(child : uid,parent:  user!.uid);
    endRunning();
  }

  void createChatRoom({required String child, required String parent}) {
    // final model = ChatModel(docId: '',id:'',senderId: parent,uid: parent,child: child,dateTime: DateTime.now().toString(),
    // lastMessage: '');
    final room = ChatRoom(roomId: '', parent: parent, child: child).newRoomMap();
    FirebaseFirestore.instance
        .collection('chats').add(room);
  }
}

enum SignUpChildreenStatus {
  pause,
  emailAlreadyInUse,
  weakPassword,
  operationNotAllowed,
  invalidEmail,
  runEmail,
  successfulEmail,
  runData,
  successfulData
}
