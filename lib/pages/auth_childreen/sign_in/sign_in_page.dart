import 'package:flutter/material.dart';
import 'package:workmanager/base_childreen/base_state.dart';
import 'package:workmanager/util/ui/common_widget/auth_switch_childreen.dart';

import '/base/base_state.dart';
import '/constants/strings.dart';
import '/routing/app_routes.dart';
import '/util/extension/extension.dart';
import '/util/ui/common_widget/auth_text_field.dart';
import '/util/ui/common_widget/link_forgot_password.dart';
import '/util/ui/common_widget/auth_switch.dart';
import '/util/ui/common_widget/primary_button.dart';
import '/util/ui/common_widget/sign_in_content.dart';
import 'sign_in_provider.dart';
import 'sign_in_vm.dart';

class SignInChildreenPage extends StatefulWidget {
  final ScopedReader watch;

  static Widget instance() {
    return Consumer(builder: (context, watch, _) {
      return SignInChildreenPage._(watch);
    });
  }

  const SignInChildreenPage._(this.watch);

  @override
  State<StatefulWidget> createState() {
    return SignInChildreenState();
  }
}

class SignInChildreenState extends BaseStateChildreen<SignInChildreenPage, SignInChildreenViewModel> {
  bool isHidden = true;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String signInChildreenStatusString = '';
  SignInChildreenStatus appStatus = SignInChildreenStatus.pause;

  final formKey = GlobalKey<FormState>();

  void togglePasswordView() {
    setState(() {
      isHidden = !isHidden;
    });
  }

  Future<void> signInChildreenClick() async {
    if (formKey.currentState!.validate()) {
      getVm().loginChildreen(emailController.text, passwordController.text);
    }
  }

  @override
  void initState() {
    super.initState();
    getVm().bsLoginStatus.listen((status) {
      setState(() {
        appStatus = status;
      });
      switch (status) {
        case SignInChildreenStatus.networkError:
          setState(() {
            signInChildreenStatusString = AppStrings.networkError;
          });
          break;
        case SignInChildreenStatus.successful:
          Get.offAndToNamed(AppRoutes.HOME);
          break;
        case SignInChildreenStatus.userDisabled:
          setState(() {
            signInChildreenStatusString = AppStrings.userDisabled;
          });
          break;
        case SignInChildreenStatus.invalidEmail:
          setState(() {
            signInChildreenStatusString = AppStrings.invalidEmail;
          });
          break;
        case SignInChildreenStatus.userNotFound:
          setState(() {
            signInChildreenStatusString = AppStrings.userNotFound;
          });
          break;
        case SignInChildreenStatus.wrongPassword:
          setState(() {
            signInChildreenStatusString = AppStrings.wrongPassword;
          });
          break;
        default:
          setState(() {
            signInChildreenStatusString = '';
          });
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildForm(),
    );
  }

  Widget buildForm() => SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 86.w),
              SignInContent(
                title: StringTranslateExtension(
                  AppStrings.welcomeBack,
                ).tr(),
                content: StringTranslateExtension(AppStrings.signInDes).tr(),
              ),
              AuthTextField(
                controller: emailController,
                label: AppStrings.username,
                hint: AppStrings.usernameHint,
                validator: (val) => val!.isNotEmpty
                    ? null
                    : StringTranslateExtension(
                        AppStrings.usernameValid,
                      ).tr(),
                enabled: !(appStatus == SignInChildreenStatus.run),
              ),
              SizedBox(height: 32.w),
              AuthTextField(
                controller: passwordController,
                label: AppStrings.password,
                hint: AppStrings.passwordHint,
                validator: (val) => val!.length < 6
                    ? StringTranslateExtension(AppStrings.passwordValid).tr()
                    : null,
                isPassword: true,
                enabled: !(appStatus == SignInChildreenStatus.run),
              ),
              const LinkForgotPassword(),
              signInChildreenStatusString.text14(color: Colors.red).tr(),
              SizedBox(height: 20.w),
              PrimaryButton(
                text: StringTranslateExtension(AppStrings.logIn).tr(),
                press: signInChildreenClick,
                disable: appStatus != SignInChildreenStatus.run,
              ),
            ],
          ),
        ),
      ).marg(24.w);

  @override
  SignInChildreenViewModel getVm() => widget.watch(viewModelProvider).state;
}
