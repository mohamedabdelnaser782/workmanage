import 'package:workmanager/base_childreen/base_view_model_childreen.dart';
import 'package:workmanager/util/providers_childreen/auth_provider.dart';

import '/base/base_view_model.dart';

class SignInChildreenViewModel extends BaseViewModelChildreen {
  SignInChildreenViewModel(ref) : super(ref);
  BehaviorSubject<SignInChildreenStatus> bsLoginStatus =
      BehaviorSubject.seeded(SignInChildreenStatus.pause);

  void init(var ref) async {
    authChildreen = ref.watch(authServicesChildreenProvider);
  }

  void loginChildreen(String email, String password) async {
    bsLoginStatus.add(SignInChildreenStatus.run);
    var status = await authChildreen.signInChildreen(email, password);
    bsLoginStatus.add(status);
  }

  @override
  void dispose() {
    bsLoginStatus.close();
    super.dispose();
  }
}

enum SignInChildreenStatus {
  pause,
  userNotFound,
  invalidEmail,
  userDisabled,
  wrongPassword,
  networkError,
  run,
  successful
}
