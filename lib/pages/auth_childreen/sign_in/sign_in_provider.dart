// ignore: depend_on_referenced_packages
import 'package:riverpod/riverpod.dart';

import 'sign_in_vm.dart';

final viewModelProvider = StateProvider.autoDispose<SignInChildreenViewModel>(
  (ref) {
    var vm = SignInChildreenViewModel(ref);
    ref.onDispose(() {
      vm.dispose();
    });
    return vm;
  },
);
