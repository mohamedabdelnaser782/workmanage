import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:workmanager/routing/app_routes.dart';


import '/constants/constants.dart';
import '/base/base_state.dart';
import '/models/project_model.dart';
import '/util/extension/dimens.dart';
import '/util/extension/widget_extension.dart';
import 'widgets/notification_home.dart';
import '../../../../util/ui/common_widget/project_card.dart';
import 'home_provider.dart';
import 'home_vm.dart';

class MenuTab extends StatefulWidget {
  final ScopedReader watch;

  final Function pressMode;

  static Widget instance({required Function pressMode}) {
    return Consumer(builder: (context, watch, _) {
      return MenuTab._(watch, pressMode);
    });
  }

  const MenuTab._(this.watch, this.pressMode);

  @override
  State<StatefulWidget> createState() {
    return MenuState();
  }
}

class MenuState extends BaseState<MenuTab, MenuViewModel> {
  late BannerAd _bannerAd;
  bool showAds = true;
  bool _isBannerAdReady = false;
  Future<InitializationStatus> _initGoogleMobileAds() {
    return MobileAds.instance.initialize();
  }

  bool isToDay = true;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(),
      appBar: buildAppBar(),
      backgroundColor: const Color.fromARGB(255, 240, 243, 236),
    );
  }

  Widget buildBody() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child:  AppStrings.notification.plain()
              .fSize(28)
              .fShadow(AppConstants.kLogoTextShadow)
              .btr(),
        ),
        Expanded(child: NotificationPage())
      ],
    );
  }

  AppBar buildAppBar() => StringTranslateExtension(AppStrings.home)
      .tr()
      .plainAppBar(color: AppColors.kWhiteBackground)
      .backgroundColor(AppColors.kPrimaryColor)
      .actions(
    [
      IconButton(
          onPressed: ()
          {
        Get.toNamed(AppRoutes.chat);
         },
          icon:Icon(Icons.chat)
      ),
    ],
  ).bAppBar();

  @override
  MenuViewModel getVm() => widget.watch(viewModelProvider).state;
}
