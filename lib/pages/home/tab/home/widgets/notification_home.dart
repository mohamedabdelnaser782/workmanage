import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workmanager/base/loading_widget.dart';
import 'package:workmanager/models/notification_model.dart';
import '/constants/constants.dart';
import '/util/extension/dimens.dart';
import '/util/extension/widget_extension.dart';
import '../../../../../util/ui/common_widget/choose_color_icon.dart';
import '../../../../../util/ui/common_widget/primary_button.dart';
import 'package:easy_localization/easy_localization.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    print('notifications_NotificationState');
    return Scaffold(
      //appBar: buildAppBar(),
      body: StreamBuilder(
        stream:  FirebaseFirestore.instance
            .collection('notifications')
            .where('uid',isEqualTo: FirebaseAuth.instance.currentUser!.uid )
            .snapshots(),
        builder: (context,AsyncSnapshot<QuerySnapshot> snapshot) {

            if(snapshot.data!=null){
            List<NotificationModel> notificationsList = [];
            snapshot.data!.docs.forEach((element) {
              NotificationModel c = NotificationModel.fromJson(element.data() as Map<String,dynamic>);
              notificationsList.add(c);
            });
            return notificationList(notificationsList);
          }
          return LoadingView();
        }
      ),
    );

  }

  Widget notificationList(List<NotificationModel> list){
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context,position){
          final item = list[position];
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.title.toString()),
              Text(item.body.toString()),
              Align(alignment: AlignmentDirectional.bottomEnd,child: Text(item.time().toString()),)
            ],
          ),
        ),
      );
    });
  }
  AppBar buildAppBar() => 'Notification'
      .plainAppBar(color: AppColors.kWhiteBackground)
      .backgroundColor(AppColors.kPrimaryColor).bAppBar();

}
