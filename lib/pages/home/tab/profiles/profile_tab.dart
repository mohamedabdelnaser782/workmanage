import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:workmanager/base/loading_widget.dart';
import 'package:workmanager/models/meta_user_model.dart';
import 'package:workmanager/models/quick_note_model.dart';
import 'package:workmanager/constants/constants.dart';
import 'package:workmanager/pages/home/tab/profiles/widgets/count_task_item.dart';
import 'package:workmanager/pages/home/tab/profiles/widgets/statistic_item.dart';
import 'package:workmanager/routing/app_routes.dart';

import '/base/base_state.dart';
import '/util/extension/dimens.dart';
import '/util/extension/widget_extension.dart';
import 'profile_provider.dart';
import 'profile_vm.dart';
import 'widgets/profile_info.dart';
import 'widgets/setting_card.dart';

class ProfileTab extends StatefulWidget {
  final ScopedReader watch;

  static Widget instance() {
    return Consumer(builder: (context, watch, _) {
      return ProfileTab._(watch);
    });
  }

  const ProfileTab._(this.watch);

  @override
  State<StatefulWidget> createState() {
    return ProfileState();
  }
}

class ProfileState extends BaseState<ProfileTab, ProfileViewModel> {
  User? localUser;

  int noteLength = 0;
  int noteSuccessfulLength = 0;

  int checkListLength = 0;
  int checkListSuccessfulLength = 0;

  int taskLength = 0;
  int taskSuccessfulLength = 0;

  @override
  void initState() {
    super.initState();
    initQuickNoteState();
    initUser();
  }

  void initQuickNoteState() {
    getVm().bsListQuickNote.listen((networkListQuickNote) {
      List<QuickNoteModel> listNote = networkListQuickNote!
          .where((quickNote) => quickNote.listNote.isEmpty)
          .toList();
      // update quick note length
      if (noteLength != listNote.length) {
        setState(() {
          noteLength = listNote.length;
        });
      }
      // update quick note successful
      var networkNoteSuccessfulLength = listNote
          .where((QuickNoteModel note) => note.isSuccessful == true)
          .length;
      if (noteSuccessfulLength != networkNoteSuccessfulLength) {
        setState(() {
          noteSuccessfulLength = networkNoteSuccessfulLength;
        });
      }

      List<QuickNoteModel> listCheckList = networkListQuickNote
          .where((quickNote) => quickNote.listNote.isNotEmpty)
          .toList();

      // update check list length
      if (checkListLength != listCheckList.length) {
        setState(() {
          checkListLength = listCheckList.length;
        });
      }

      // update quick note successful
      var networkQuickNoteSuccessfulLength =
          listCheckList.where((element) => element.isSuccessful).length;
      if (checkListSuccessfulLength != networkQuickNoteSuccessfulLength) {
        setState(() {
          checkListSuccessfulLength = networkQuickNoteSuccessfulLength;
        });
      }
    });

    getVm().bsListTask.listen((value) {
      if (value != null) {
        setState(() {
          taskSuccessfulLength =
              value.where((element) => element.completed).toList().length;
          taskLength = value.length;
        });
      }
    });
  }

  void initUser() {
    getVm().getUser().listen((networkUser) {
      // init local user
      if (networkUser != null) {
        if (localUser == null) {
          setState(() {
            localUser = networkUser;
          });
        } else {
          if (localUser!.photoURL != networkUser.photoURL) {
            localUser!.updatePhotoURL(networkUser.photoURL);
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: localUser == null ? 'Loading'.desc() : buildBody(),
      appBar: buildAppBar(),
    );
  }

  Widget buildBody() {

    return StreamBuilder(


        stream: FirebaseFirestore.instance
            .collection('user')
            .doc(FirebaseAuth.instance.currentUser!.uid)
            .snapshots(),
        builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.data != null) {
            MetaUserModel user = MetaUserModel.fromFirestore(snapshot.data!);


            return Container(
              color: const Color.fromARGB(255, 240, 243, 236),
              height: screenHeight,
              width: screenWidth,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 20.w),
                    buildCardInfo(),
                    SizedBox(height: 24.w),
                    buildListCountTask(),
                    SizedBox(height: 24.w),
                    user .isChild() ? Container() : buildStatistic(),
                  ],
                ),
              ),
            );
          }else{
            return LoadingView();
          }
      }
    );
  }

  AppBar buildAppBar() => StringTranslateExtension('profiles')
      .tr()
      .plainAppBar(color: AppColors.kWhiteBackground)
      .backgroundColor(AppColors.kPrimaryColor)
      .bAppBar();

  Widget buildCardInfo() {
    return Container(
      width: screenWidth,
      height: 190.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.r),
        color: Colors.white,
        boxShadow: AppConstants.kBoxShadow,
      ),
      child: StreamBuilder<infoStatus>(
        stream: getVm().bsInfoStatus,
        builder: (context, snapshot) {
          if (snapshot.data == infoStatus.setting) {
            return SettingCard(
              pressToProfile: () => getVm().changeInfoStatus(infoStatus.info),
              pressSignOut: () {
                getVm().signOut();
                Get.offAndToNamed(AppRoutes.SIGN_IN);
              },
              pressUploadAvatar: getVm().uploadAvatar,
            );
          }
          return ProfileInfo(
            user: localUser!,
            press: () => getVm().changeInfoStatus(infoStatus.setting),
            createTask: noteLength + checkListLength + taskLength,
            completedTask: noteSuccessfulLength +
                checkListSuccessfulLength +
                taskSuccessfulLength,
          );
        },
      ),
    ).pad(0, 16);
  }

  Widget buildSetting() {
    return Container();
  }

  Widget buildListCountTask() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          CountTaskItem(
            text: AppConstants.kStatisticTitle[0],
            task: taskLength,
          ).pad(0, 10, 0),
          CountTaskItem(
            text: AppConstants.kStatisticTitle[1],
            task: noteLength,
            color: AppColors.kSplashColor[1],
          ).pad(0, 10, 0),
          CountTaskItem(
            text: AppConstants.kStatisticTitle[2],
            task: checkListLength,
            color: AppColors.kSplashColor[2],
          ),
        ],
      ).pad(20, 20, 0),
    );
  }

  Widget buildStatistic() {
    return Container(
      width: 343.w,
      height: 105.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.r),
        boxShadow: AppConstants.kBoxShadow,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AppStrings.addChildreen
              .plain()
              .fSize(18)
              .lHeight(21.09)
              .weight(FontWeight.bold)
              .b()
              .tr()
              .pad(0, 0, 16, 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            IconButton(
              iconSize: 40,
                onPressed: (){
                  Get.offNamed(AppRoutes.SIGNUPCHILDREEN);
                },
                icon: Icon(Icons.add))
            ],
          )
        ],
      ).pad(0, 24),
    ).pad(0, 16);
  }

  @override
  ProfileViewModel getVm() => widget.watch(viewModelProvider).state;
}
