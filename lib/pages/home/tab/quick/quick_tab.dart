import 'package:flutter/material.dart';
import 'package:workmanager/base/widgets.dart';
import 'package:workmanager/models/award_model.dart';
import 'package:workmanager/models/meta_user_model.dart';
import 'package:workmanager/models/task_model.dart';
import '/base/base_state.dart';
import '/constants/app_colors.dart';
import '/routing/app_routes.dart';
import '/models/quick_note_model.dart';
import '/util/extension/dimens.dart';
import '/util/extension/widget_extension.dart';
import '../../../../util/ui/common_widget/quick_note_card.dart';
import 'quick_provider.dart';
import 'quick_vm.dart';

class QuickTab extends StatefulWidget {
  final ScopedReader watch;

  static Widget instance() {
    return Consumer(builder: (context, watch, _) {
      return QuickTab._(watch);
    });
  }

  const QuickTab._(this.watch);

  @override
  State<StatefulWidget> createState() {
    return QuickState();
  }
}

class QuickState extends BaseState<QuickTab, QuickViewModel> {
  bool isFullQuickNote = false;
  @override
  Widget build(BuildContext context) {
    return UserWidget(builder: (BuildContext context, MetaUserModel user) {
      return Scaffold(
        body: buildBody(user),
        appBar: buildAppBar(),
      );
    },

    );
  }

  Widget buildBody(MetaUserModel user) {
    return Container(
      color: AppColors.kWhiteBackground,
      height: screenHeight,
      width: screenWidth,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 32.w),
            StreamBuilder<List<TaskModel>?>(
                stream: getVm().bsListQuickNote,
                builder: (context, snapshot) {
                  print('');
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Text("Loading");
                  }

                  List<TaskModel> data = snapshot.data!;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      if (data.isEmpty) buildNoneNote(),
                      for (int i = 0; i < data.length; i++)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('awards for  '+data[i].title),
                                  const SizedBox(height: 8,),
                                  awardMessage(data[i].completed , user),
                                  const SizedBox(height: 8,),
                                  awardsList(data[i].awards??[],data[i].completed),

                                ],
                              ),
                            ),
                          ),
                        )
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget awardsList(List<AwardModel> awards , bool complete){
    return Wrap(
      spacing: 8.0, // gap between adjacent chips
      runSpacing: 2,
      children: awards.map((e) => buildChip(e.name! , complete)).toList(),
    );
  }

 Widget buildChip(String name,bool complete) {
    return Chip(
      materialTapTargetSize: MaterialTapTargetSize.padded,
      visualDensity: const VisualDensity(horizontal: 0.0, vertical: -4),
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6))),
      backgroundColor: complete ? const Color(0xffffbf47): Colors.grey[400],
      label: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(name,),
        ],
      ),
    );
  }
  Widget buildAward({
    required String text,
    required bool check,
  }) {
    return Row(
      children: [
        Container(
          width: 12.w,
          height: 12.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3.w),
            color: check ? AppColors.kInnerBorder : Colors.white,
            border: Border.all(
              color: AppColors.kInnerBorder,
            ),
          ),
        ).pad(0, 11, 0),
        text
            .plain()
            .fSize(16)
            .lHeight(19.7)
            .decoration(
            check ? TextDecoration.lineThrough : TextDecoration.none)
            .b(),
      ],
    );
  }
  AppBar buildAppBar() => 'Awards'
          .plainAppBar(color: AppColors.kWhiteBackground)
          .backgroundColor(AppColors.kPrimaryColor).bAppBar();

  Widget buildNoneNote() =>
      'You are not have a awards, \n complete a task to get your awards'.desc();

  @override
  QuickViewModel getVm() => widget.watch(viewModelProvider).state;

  Widget awardMessage(bool completed, MetaUserModel user) {
    if(user.isChild()){
     return  completed ? const Text('Congratulation you have earned task awards',
        style: TextStyle(fontStyle: FontStyle.italic,
            color: Color(0xffffb115),fontWeight: FontWeight.bold
            ,fontSize: 16),):
      const Text('you need to complete this if you need earn awards',
        style: TextStyle(fontSize: 14),);
    }else{
     return completed ? Text(' your child have earned this task awards',
        style: TextStyle(fontWeight: FontWeight.bold
            ,fontSize: 14),):
      const Text('your child will earn this awards when complete the task',
        style: TextStyle(fontSize: 14),);
    }
  }
}
