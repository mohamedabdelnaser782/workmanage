import 'package:workmanager/models/award_model.dart';
import 'package:workmanager/models/task_model.dart';

import '/base/base_view_model.dart';
import '/models/quick_note_model.dart';

class QuickViewModel extends BaseViewModel {
  BehaviorSubject<List<TaskModel>?> bsListQuickNote =
      BehaviorSubject<List<TaskModel>>();

  QuickViewModel(ref) : super(ref) {
    firestoreService.currentUserObjectStream().listen((event) {
      firestoreService.awardsStream(!event.isChild()).listen((event) {
        bsListQuickNote.add(event);
      });
    });

  }

  void successfulQuickNote(QuickNoteModel quickNoteModel) {
    // update to local
    quickNoteModel.isSuccessful = true;
    // update to network
    firestoreService.updateQuickNote(user!.uid, quickNoteModel);
  }

  void checkedNote(QuickNoteModel quickNoteModel, int idNote) {
    // check note
    quickNoteModel.listNote[idNote].check = true;
    // update note to network
    firestoreService.updateQuickNote(user!.uid, quickNoteModel);
  }

  void deleteNote(QuickNoteModel quickNoteModel) async {
    // delete note in network
    await firestoreService.deleteQuickNote(user!.uid, quickNoteModel.id);
  }
}
