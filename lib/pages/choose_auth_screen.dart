import 'package:flutter/material.dart';
import 'package:workmanager/constants/resources.dart';
import 'package:workmanager/routing/app_routes.dart';
import 'package:workmanager/util/extension/extension.dart';

import '../base_childreen/base_state.dart';
import '../constants/images.dart';
import '../constants/strings.dart';
import '../util/extension/dimens.dart';

class ChoosePage extends StatefulWidget {

  const ChoosePage({Key? key}) : super(key: key);

  @override
  State<ChoosePage> createState() => _ChoosePageState();
}

class _ChoosePageState extends State<ChoosePage> {

  bool button = false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return Container(
      color: Colors.white,
      width: screenWidth,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Text(
              "Welcome Back ! ",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF2661FA),
                  fontSize: 36),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(height: 10),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              "Choose Your Identity ",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF2661FA),
                  fontSize: 15),
              textAlign: TextAlign.left,
            ),
          ),
          Row(
            children: [
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: ElevatedButton(
                  onPressed: () {
                    Get.offNamed(AppRoutes.SIGNINCHILDREEN);
                  },
                  style: raisedButtonStyle,
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    width: 100,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      color: button
                          ? Colors.grey
                          : Color.fromARGB(255, 255, 136, 34),
                    ),
                    padding: const EdgeInsets.all(0),
                    child: Text(
                      "Childreen",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: ElevatedButton(
                  onPressed: () {
                    Get.offNamed(AppRoutes.SIGN_IN);
                  },

               /*   shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(0),*/
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    width: 100,
                    decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      color: button
                          ? Color.fromARGB(255, 255, 136, 34)
                          : Colors.grey,
                    ),
                    padding: const EdgeInsets.all(0),
                    child: Text(
                      "Parent",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
