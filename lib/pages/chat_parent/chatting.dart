import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:intl/intl.dart';
import 'package:workmanager/chat/message.dart';
import 'package:workmanager/models/chat_room.dart';
import 'package:workmanager/models/meta_user_model.dart';

class Chatting extends StatelessWidget {
  // final AppUser appUser;
  // final CurrentAppUser currentUser;
  final MetaUserModel childreenModel;
  final MetaUserModel currentUser;
  final ChatRoom room;

  Chatting({Key? key, required this.childreenModel, required this.currentUser , required this.room
      // @required this.appUser,
      // @required this.currentUser,
      })
      : super(key: key);

  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    print('/////////////////////////////');
    print(FirebaseAuth.instance.currentUser!.uid);
    final roomId = room.roomId ;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.black26,
          leading: null,
          centerTitle: true,
          title: Text(
            childreenModel.displayName,
            style: TextStyle(
                color: Color.fromARGB(255, 255, 136, 34), fontSize: 20),
            textAlign: TextAlign.left,
          ),
        ),
        floatingActionButton: MessageField(
          doctorModel: childreenModel,
          room: room,
          // appUser: appUser,
          currentUser: currentUser,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        body: Column(
          children: [
            Container(
              child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('chats')
                    .doc(roomId)
                    .collection('messages')
                    .orderBy('createdAt', descending: true)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  final List<Message> messagesList = [];
                  snapshot.data!.docs.forEach((element) {
                    messagesList.add(Message.fromMap(element.data()as Map<String,dynamic>));
                  });
                  // updateStatus(snapshot.data.docs);

                  return messagesList.isEmpty
                      ? const Expanded(
                          child: Center(
                            child: Text('Start the Conversation'),
                          ),
                        )
                      : Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 70.0),
                            child: ListView.builder(
                              reverse: true,
                              shrinkWrap: true,
                              itemCount: messagesList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return MessageTile(
                                  message: messagesList[index],
                                  doctorModel: childreenModel,
                                  currentUser: currentUser,
                                );
                              },
                            ),
                          ),
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  updateStatus(List<QueryDocumentSnapshot> docs) async {
    for (int i = 0; i < docs.length; i++) {
      // if ((docs[i].data() as Map<String, dynamic>)['ownerId'].toString() !=
      //     currentUser.userId) print("${docs[i].reference} === > status : true");
      // await docs[i].reference.update({'seen': true});
    }
  }
}

class MessageTile extends StatelessWidget {
  final MetaUserModel currentUser;
  final MetaUserModel doctorModel;
  final Message message;

  const MessageTile(
      {Key? key,
      required this.doctorModel,
      required this.message,
      required this.currentUser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Row(
      mainAxisAlignment:
          //  message.ownerId == currentUser.userId
          message.uId == currentUser.uid
              ? MainAxisAlignment.end
              : MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment:
                // message.ownerId == currentUser.userId
                message.uId == currentUser.uid
                    ? MainAxisAlignment.end
                    : MainAxisAlignment.start,
            children: [
              //  (currentUser.userId != message.ownerId)
              message.uId != currentUser.uid
                  ? CircleAvatar(
                      radius: 20,
                      backgroundColor: Colors.grey,
                      child: Icon(
                        Icons.account_circle,
                        size: 40,
                        color: Colors.white,
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 5),
                      child: Text(
                        DateTime.parse(message.createdAt!).day ==
                                Timestamp.now().toDate().day
                            ? DateFormat('hh:mm a ')
                                .format(DateTime.parse(message.createdAt!))
                            : DateFormat('dd MMM, hh:mm a  ')
                                .format(DateTime.parse(message.createdAt!)),
                        style: TextStyle(
                            fontSize: size.width * 0.02,
                            color: const Color(0xff6B6B6B)),
                      ),
                    ),
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment:
                      //  message.ownerId == currentUser.userId
                      message.uId == currentUser.uid
                          ? CrossAxisAlignment.end
                          : CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      message.uId!.startsWith(currentUser.uid)
                          // message.ownerId == currentUser.userId
                          ? '  ${currentUser.displayName}'
                          : '  ${doctorModel.displayName}',
                      style: TextStyle(
                          fontSize: size.width * 0.04,
                          fontWeight: FontWeight.w500,
                          color: const Color(0xff6B6B6B)),
                    ),
                    Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          padding: EdgeInsets.all(7),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Color(0xffEFEDED)),
                          child: Text(
                            message.message!,
                            style: TextStyle(fontSize: size.width * 0.04),
                          ),
                        ),
                        if (message.uId!.startsWith(doctorModel.uid))
                          // (currentUser.userId != message.ownerId)
                          Text(
                            DateTime.parse(message.createdAt!).day ==
                                    Timestamp.now().toDate().day
                                ? DateFormat(' hh:mm a')
                                    .format(DateTime.parse(message.createdAt!))
                                : DateFormat(' dd MMM, hh:mm a')
                                    .format(DateTime.parse(message.createdAt!)),
                            style: TextStyle(
                                fontSize: size.width * 0.02,
                                color: const Color(0xff6B6B6B)),
                          ),
                      ],
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                  ],
                ),
              ),
              if (message.uId!.startsWith(currentUser.uid))
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Color(0xFF527DAA),
                  child: Icon(
                    Icons.account_circle,
                    size: 40,
                    color: Colors.white,
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _image(String url) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: [
          CircleAvatar(
            radius: 25,
            //backgroundColor: AppColor.appColor,
          ),
          Padding(
            padding: EdgeInsets.only(top: 1.5, left: 1.5),
            child: Container(
              height: 47,
              width: 47,
              child: CachedNetworkImage(
                imageUrl: url,
                imageBuilder: (context, imageProvider) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => (SafeArea(
                                child: Scaffold(
                                  backgroundColor: Colors.black,
                                  body: Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover)),
                                  ),
                                ),
                              ))),
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0.0),
                    child: Container(
                      child: CircleAvatar(
                        radius: 17,
                        backgroundImage: imageProvider,
                      ),
                    ),
                  ),
                ),
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => const Icon(
                  Icons.error_outline,
                  color: Colors.red,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MessageField extends StatelessWidget {
  final MetaUserModel currentUser;
  final MetaUserModel doctorModel;
  final ChatRoom room ;
  final TextEditingController controller = TextEditingController();

  MessageField({
    Key? key,
    required this.room,
    required this.doctorModel,
    required this.currentUser,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 0.0),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        alignment: Alignment.center,
        height: size.height * 0.08,
        width: size.width * 0.95,
        child: Row(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: Color.fromARGB(
                        255, 255, 136, 34), // red as border color
                  ),
                ),
                child: TextField(
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  controller: controller,
                  expands: true,
                  textInputAction: TextInputAction.newline,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(12, 8, 0, 8),
                    suffixIcon: IconButton(
                      padding: EdgeInsets.zero,
                      icon: const RotationTransition(
                        turns: AlwaysStoppedAnimation(315 / 360),
                        child: Icon(
                          Icons.send,
                          color: Color.fromARGB(255, 255, 136, 34),
                        ),
                      ),
                      onPressed: () async {
                        final roomId = room.roomId ;
                        if (controller.text.trim().isEmpty) {
                          Fluttertoast.showToast(msg: 'Please write a message');
                        } else {
                          DocumentSnapshot<Map<String, dynamic>> doc =
                              await FirebaseFirestore.instance
                                  .collection('chats')
                                  .doc(roomId)
                                  .get();
                          if (doc.exists) {
                            FirebaseFirestore.instance
                                .collection('chats')
                                .doc(roomId)
                                .collection('messages')
                                .add(
                                  Message(
                                    uId: currentUser.uid,
                                    message: controller.text.trim(),
                                    seen: false,
                                    createdAt: Timestamp.now()
                                        .toDate()
                                        .toIso8601String(),
                                  ).toMap(),
                                );
                            FirebaseFirestore.instance
                                .collection('chats')
                                .doc(roomId)
                                .update({
                              'last_message_time':
                                  Timestamp.now().toDate().toIso8601String(),
                              'last_message': controller.text.trim()
                            });
                          } else {
                            FirebaseFirestore.instance
                                .collection('chats')
                                .doc(roomId)
                                .set({
                              'uid': FirebaseAuth.instance.currentUser!.uid,
                              'doc_id': doctorModel.uid,
                              'created_at':
                                  Timestamp.now().toDate().toIso8601String(),
                              'last_message': '',
                              'sender_id': FirebaseAuth.instance.currentUser!.uid
                            });
                            FirebaseFirestore.instance
                                .collection('chats')
                                .doc(doctorModel.uid + '-' + currentUser.uid)
                                .collection('messages')
                                .add(
                                  Message(
                                    //id: doctorModel.uid + '-' + currentUser.uid,
                                    uId: currentUser.uid,
                                  //  docId: doctorModel.uid,
                                    message: controller.text.trim(),
                                    seen: false,
                                    createdAt: Timestamp.now()
                                        .toDate()
                                        .toIso8601String(),
                                  ).toMap(),
                                );
                            FirebaseFirestore.instance
                                .collection('chats')
                                .doc(doctorModel.uid + '-' + currentUser.uid)
                                .update({
                              'last_message_time':
                                  Timestamp.now().toDate().toIso8601String(),
                              'last_message': controller.text.trim(),
                              'sender_id': FirebaseAuth.instance.currentUser!.uid
                            });
                          }

                          // FirebaseFirestore.instance
                          //     .collection('users')
                          //     .doc(currentUser.userId)
                          //     .collection(appUser.userId)
                          //     .add(
                          //       Message(
                          //         ownerId: currentUser.userId,
                          //         message: controller.text.trim(),
                          //         seen: false,
                          //         createdAt: Timestamp.now()
                          //             .toDate()
                          //             .toIso8601String(),
                          //       ).toMap(),
                          //     );
                          // FirebaseFirestore.instance
                          //     .collection('users')
                          //     .doc(currentUser.userId)
                          //     .update(
                          //   {
                          //     "messages":
                          //         FieldValue.arrayUnion([appUser.userId]),
                          //   },
                          // );

                          //   FirebaseFirestore.instance
                          //       .collection('users')
                          //       .doc(appUser.userId)
                          //       .update(
                          //     {
                          //       "messages":
                          //           FieldValue.arrayUnion([currentUser.userId]),
                          //     },
                          //   );
                          controller.clear();
                        }
                      },
                    ),
                    hintText: "Write Message...",
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
