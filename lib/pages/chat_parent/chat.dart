
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:workmanager/chat/message.dart';
import 'package:workmanager/models/chat_room.dart';
import 'package:workmanager/models/meta_user_model.dart';
import 'package:workmanager/pages/chat_parent/chatting.dart';
import 'package:workmanager/util/providers/fire_store_provider.dart';
import 'package:workmanager/util/services/fire_store_services.dart';

class ChatScreen extends StatelessWidget {
  //final CurrentAppUser currentuser;

  const ChatScreen({
    Key ?key,
    //@required this.currentuser
  }) : super(key: key);
  Future<MetaUserModel> getCurrentUser() async {
    DocumentSnapshot<Map<String, dynamic>> doc = await FirebaseFirestore
        .instance
        .collection('user')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .get();
    print('getCurrentUser ${doc.data()}');
    MetaUserModel currentUser = MetaUserModel.fromFirestore(doc as DocumentSnapshot);
    return currentUser;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Chats',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          elevation: 0,
          backgroundColor: Color(0xFF527DAA),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: StreamBuilder(
            stream:FirestoreService(FirebaseFirestore.instance).currentUserStream(),
            builder:
                (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else {
                MetaUserModel user =
                MetaUserModel.fromFirestore(snapshot.data!);
                return SizedBox(
                  child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('chats')
                          .where(user.isChild() ? 'child':'parent',isEqualTo: FirebaseAuth.instance.currentUser!.uid )
                          .snapshots(),
                      builder: (context, snap) {
                        if (snap.connectionState == ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        List<String> ids = [];
                        /*   snap.data!.doc.forEach((element) {
                      ids.add(element.data()['uid']);
                    });*/
                        List<ChatRoom> chatsList = [];
                        snap.data!.docs.forEach((element) {
                          ChatRoom c = ChatRoom.fromJson(element.data() as Map<String,dynamic>);
                          c.roomId = element.id;
                          c.participant = user.isChild()?c.parent : c.child;

                          chatsList.add(c);
                        });
                        chatsList.forEach((element) {
                          print('chatsList ${element.toMap()}');

                        });
                        return chatsList.isEmpty
                            ? Center(
                          child: Text('No Chat available'),
                        )
                            : Column(
                            children: chatsList
                                .map((e) => messageItem(context, e))
                                .toList());
                      }),
                );
              }
          }
        ));
  }

  Widget messageItem(
    BuildContext context,
      ChatRoom e) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('user')
              .doc(e.participant)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            MetaUserModel user =
            MetaUserModel.fromFirestore(snapshot.data!);

            return ListTile(
              onTap: () {
                FirebaseFirestore.instance
                    .collection('chats')
                    .doc(e.roomId)
                    .update({'dr_seen': true});
                onMsgItemtab(user , e, context);
              },
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.displayName,
                  ),
                  StreamBuilder(
                    stream:
                    FirebaseFirestore.instance
                        .collection('chats')
                        .doc(e.roomId)
                        .collection('messages')
                        .orderBy('createdAt', descending: true)
                        .snapshots(),
                    builder: (context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      final message = snapshot.data?.docs.first;

                      print('chat_messages ${message?.data()}');
                        return message!=null ? messageTextItem(message ,context):Container();
                    }
                  ),

                ],
              ),
              leading: CircleAvatar(
                radius: 20,
                backgroundColor: Color(0xFF527DAA),
                child: Icon(
                  Icons.account_circle,
                  size: 40,
                  color: Colors.white,
                ),
              ),
            );
          }),
    );
  }


  Widget messageTextItem(DocumentSnapshot snapshot  , BuildContext context ) {
    final messageModel = Message.fromMap(snapshot.data() as Map<String , dynamic>);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            //width: MediaQuery.of(context).size.width - 120,
            child: Text(
              messageModel.message??'',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                  fontSize: 13, fontWeight: FontWeight.w300),
            ),

          ),
        ),
        messageModel.createdAt !=null ? Text(
          timeago.format(DateTime.parse(messageModel.createdAt!),
              locale: 'en_long'),
          style: const TextStyle(
              fontSize: 9, fontWeight: FontWeight.w300),
        ):Container()
      ],
    );
  }

  Future<void> onMsgItemtab(MetaUserModel e, ChatRoom room,BuildContext context) async {
    MetaUserModel u = await getCurrentUser();
    Navigator.push(context, MaterialPageRoute
      (builder: (context) => Chatting(
        childreenModel: e,
        room: room,
        currentUser: u)));
  }

}
