import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:workmanager/pages/auth_childreen/sign_in/sign_in_vm.dart';
import 'package:workmanager/pages/auth_childreen/sign_up/sign_up_vm.dart';

import '../../pages/auth/reset_password/reset_password_vm.dart';
import '/constants/app_colors.dart';
import '/pages/auth/forgot_password/forgot_password_vm.dart';
import '/pages/auth/sign_in/sign_in_vm.dart';
import '/pages/auth/sign_up/sign_up_vm.dart';

class AuthenticationServiceChildreen {
  final FirebaseAuth _firebaseAuth;

  AuthenticationServiceChildreen(this._firebaseAuth);

  Stream<User?> get authStateChange => _firebaseAuth.authStateChanges();

  Future<SignInChildreenStatus> signInChildreen(String email, String password) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      servicesResultPrint('Sign In Successful');
      return SignInChildreenStatus.successful;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-email':
          servicesResultPrint('Invalid email');
          return SignInChildreenStatus.invalidEmail;
        case 'user-disabled':
          servicesResultPrint('User disabled');
          return SignInChildreenStatus.userDisabled;
        case 'user-not-found':
          servicesResultPrint('User not found');
          return SignInChildreenStatus.userNotFound;
        case 'wrong-password':
          servicesResultPrint('Wrong password');
          return SignInChildreenStatus.wrongPassword;
        default:
          return SignInChildreenStatus.wrongPassword;
      }
    }
  }

  Future<SignUpChildreenStatus> signUpChildreen(String email, String password , String name,{required  Function(String uid ) onSuccess} ) async {
    FirebaseApp app = await Firebase.initializeApp(
        name: 'Secondary', options: Firebase.app().options);
   final _auth =  FirebaseAuth.instanceFor(app: app);
    try {
      await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      _auth.currentUser!.updateDisplayName(name);
      final id = _auth.currentUser!.uid;
      await onSuccess(id);
      servicesResultPrint('Sign up successful');
      return SignUpChildreenStatus.successfulEmail;
    } on FirebaseAuthException catch (e) {
      await app.delete();
      switch (e.code) {
        case 'email-already-in-use':
          servicesResultPrint('Email already in user');
          return SignUpChildreenStatus.emailAlreadyInUse;
        case 'operation-not-allowed':
          servicesResultPrint('Operation not allowed');
          return SignUpChildreenStatus.operationNotAllowed;
        case 'invalid-email':
          servicesResultPrint('Invalid email');
          return SignUpChildreenStatus.invalidEmail;
        case 'weak-password':
          servicesResultPrint('Weak password');
          return SignUpChildreenStatus.weakPassword;
        default:
          return SignUpChildreenStatus.weakPassword;
      }
    }
  }

  Future<ForgotPasswordStatus> sendRequest(String email) async {
    try {
      await _firebaseAuth.sendPasswordResetEmail(
        email: email,
        actionCodeSettings: ActionCodeSettings(
          url: 'https://your reset link',
          androidPackageName: 'com.example.khawla',
          androidInstallApp: true,
          handleCodeInApp: true,
        ),
      );
      servicesResultPrint('Password reset email sent');
      return ForgotPasswordStatus.successful;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-email':
          servicesResultPrint('Invalid email');
          return ForgotPasswordStatus.invalidEmail;
        case 'user-disabled':
          servicesResultPrint('User disabled');
          return ForgotPasswordStatus.userDisabled;
        case 'user-not-found':
          servicesResultPrint('User not found');
          return ForgotPasswordStatus.userNotFound;
        case 'too-many-requests':
          servicesResultPrint('Too many requests');
          return ForgotPasswordStatus.tooManyRequest;

        default:
          return ForgotPasswordStatus.pause;
      }
    }
  }

  Future<ResetPasswordStatus> changePassword(
      String code, String password) async {
    try {
      await _firebaseAuth.confirmPasswordReset(
          code: code, newPassword: password);
      servicesResultPrint('Reset password successful', isToast: false);
      return ResetPasswordStatus.successful;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-action-code':
          return ResetPasswordStatus.invalidActionCode;
        case 'user-disabled':
          return ResetPasswordStatus.userDisabled;
        case 'user-not-found':
          return ResetPasswordStatus.userNotFound;
        case 'expired-action-code':
          return ResetPasswordStatus.expiredActionCode;
        case 'weak-password':
          return ResetPasswordStatus.weakPassword;
        default:
          return ResetPasswordStatus.pause;
      }
    }
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
    servicesResultPrint('Sign out');
  }

  User? currentUser() {
    if (_firebaseAuth.currentUser == null) {
      servicesResultPrint('Current user not found', isToast: false);
      return null;
    }
    servicesResultPrint("Current user: ${_firebaseAuth.currentUser!.uid}",
        isToast: false);
    return _firebaseAuth.currentUser;
  }

  void servicesResultPrint(String result, {bool isToast = true}) async {
    if (isToast) {
      await Fluttertoast.showToast(
        msg: result,
        timeInSecForIosWeb: 2,
        backgroundColor: AppColors.kWhiteBackground,
        textColor: AppColors.kText,
      );
    }
  }
}
