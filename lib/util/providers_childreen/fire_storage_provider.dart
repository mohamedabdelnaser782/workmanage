import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:workmanager/util/services_childreen/fire_storage_services.dart';

import '../services/fire_storage_services.dart';

final firebaseStorageChildreenProvider = Provider<FirebaseStorage>((ref) {
  return FirebaseStorage.instance;
});

final fireStorageChildreenServicesProvider = Provider<FireStorageChildreenService>((ref) {
  return FireStorageChildreenService(ref.read(firebaseStorageChildreenProvider));
});
