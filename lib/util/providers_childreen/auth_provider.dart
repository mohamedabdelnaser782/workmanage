import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:workmanager/util/services_childreen/auth_services.dart';

import '../services/auth_services.dart';


final firebaseAuthChildreenProvider = Provider<FirebaseAuth>((ref) {
  return FirebaseAuth.instance;
});


final authServicesChildreenProvider = Provider<AuthenticationServiceChildreen>((ref) {
  return AuthenticationServiceChildreen(ref.read(firebaseAuthChildreenProvider));
});

final authStateChildreenProvider = StreamProvider<User?>((ref) {
  return ref.read(authServicesChildreenProvider).authStateChange;
});
