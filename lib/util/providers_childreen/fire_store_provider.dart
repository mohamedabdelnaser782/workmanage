import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:workmanager/util/services_childreen/fire_store_services.dart';

import '../services/fire_store_services.dart';

final firebaseFirestoreChildreenProvider = Provider<FirebaseFirestore>((ref) {
  return FirebaseFirestore.instance;
});

final firestoreServicesChildreenProvider = Provider<FirestoreChildreenService>((ref) {
  return FirestoreChildreenService(ref.read(firebaseFirestoreChildreenProvider));
});
