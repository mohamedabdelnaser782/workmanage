import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'package:http/src/response.dart';
import 'package:workmanager/util/services/fire_store_services.dart';

class FirestoreMessagingService {
  // ignore: unused_field
  final FirebaseMessaging _firebaseMessaging;
  FirestoreMessagingService(this._firebaseMessaging);
  String currentToken = '';
  String serverKey =
  'AAAATwlFyLs:APA91bEmUZNILyf2JAKXSaiXh_lGodhOIhdNFVIpN7NqSX6_U8LPPU67FvRjiT2X70oR-oxoWtWnIfbysysBmjssUWOR9dwpAJiEYpUqU8GjypvRy3RBak4HkvELfATY7IKAK1IANKcg';
//      'BC0YIsJvvZUXrzJKMKzZNe8cPjapG-mbDhNXV5czMDfaIizH6YoTqhFhvh6YuB_We2dIWczD_tCSWvjIvT8Qizo';

  getToken() {
    String? uid = FirebaseAuth.instance.currentUser?.uid;
    if (uid != null) {
      FirebaseMessaging.instance
          .getToken()
          .then((value) => {currentToken = value!, saveToken(uid, value)});
    }
  }

  saveToken(String uid, String token) {
    FirebaseFirestore.instance
        .collection("user")
        .doc(uid)
        .update({"messingToken": token});
  }
  saveNotification(String title, String body ,  String token) {
    FirestoreService( FirebaseFirestore.instance)
        .getUserByFcmToken(token).then((user) => {
    FirebaseFirestore.instance
        .collection("notifications").doc().set({
      'title':title,
      'body':body,
      'uid':user.uid,
      'createdAt': Timestamp.now().toDate().toIso8601String()
    })
    });

  }

  Future<String> sendPushMessaging(
      String desToken, String title, String body) async {
    // ignore: unnecessary_null_comparison
    print('sendPushMessaging start');
    if (desToken == null) {
      return "no token exists";
    }

    try {
      // ignore: unused_local_variable
      Response res = await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'authorization': 'key=$serverKey',
        },
        body: jsonEncode({
          'token': currentToken,
          'notification': <String, dynamic>{'title': title, 'body': body},
          "android": {"priority": "normal"},
          "apns": {
            "headers": {"apns-priority": "5"}
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '1',
            'status': 'done'
          },
          "fcm_options": {
            //'send': '$currentToken',
          },
          'to': desToken,
        }),
      );

      print('sendPushMessaging response ${res.body}');
      saveNotification(title,body,desToken);
      return "success send message";
    } catch (e) {
      print('sendPushMessaging $e');
      return "error while sending message";
    }
  }
}
