import 'package:flutter/material.dart';

class Message {
  final String? uId;
  final String? message;
  final String? createdAt;
  bool? seen;

  Message(
      {
      this.uId,
      this.message,
      this.createdAt,
       this.seen});

  // Message copyWith(
  //     {String ownerId, String message, String createdAt, String seen}) {
  //   return Message(
  //     ownerId: ownerId ?? this.ownerId,
  //     message: message ?? this.message,
  //     createdAt: createdAt ?? this.createdAt,
  //     seen: seen ?? this.seen,
  //   );
  // }

  Map<String, dynamic> toMap() {
    return {
      //'id': id,
      'uid': uId,
     // 'doc_id': docId,
      'message': message,
      'createdAt': createdAt,
      'seen': seen
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    if (map == null) return Message();

    return Message(
      //  id: map['id'],
        uId: map['uid'],
       // docId: map['doc_id'],
        message: map['message'],
        createdAt: map['createdAt'],
        seen: map['seen'] ?? true);
  }


}
